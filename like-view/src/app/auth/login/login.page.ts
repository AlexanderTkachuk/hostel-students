import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/shared/services/api.service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserObject } from 'src/app/shared/interfaces/user.interface';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  loginForm: FormGroup;
  constructor(
    private httpService: ApiService,
    private userService: UserService,
    public toastController: ToastController,
    private router: Router) {
    this.loginForm = new FormGroup({
      name: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
      secondName: new FormControl('', [Validators.required, this.noWhitespaceValidator]),
      email: new FormControl('', [Validators.required, Validators.email, this.noWhitespaceValidator]),
      password: new FormControl('', [Validators.required, this.noWhitespaceValidator])
    });
  }
  public noWhitespaceValidator(control: FormControl) {
    if (control && control.value && !control.value.replace(/\s/g, '').length) {
      control.setValue('');
    }
    return null;
  }
  login() {
    console.log(this.loginForm.value);
    this.httpService.post('http://localhost:8000/api/login/', {
      ...this.loginForm.value
    }).subscribe(
      (user: UserObject) => {
        this.userService.userobject.next(user);
        this.userService.token.next(user.token);
        this.router.navigate(['/home']);
      },
      (err: HttpErrorResponse) => this.presentToast(err.error.message)
    );
  }

  async presentToast(text: string) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
  toToRegister() {
    this.router.navigate(['/register']);
  }

}
