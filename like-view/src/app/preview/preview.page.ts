import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.page.html',
  styleUrls: ['./preview.page.scss'],
})
export class PreviewPage {
  @ViewChild('slides', { static: true }) slides: IonSlides;
  currentIndex$: BehaviorSubject<number> = new BehaviorSubject(0);
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  slideText: string;
  constructor(private router: Router) {
    this.slideText = 'Далее';
  }

  slideChanged(e: any) {
    this.slides.getActiveIndex().then((index: number) => {
      this.currentIndex$.next(index);
      console.log(index);
    });
  }
  nextSlide() {
    this.slides.slideNext();
    if (this.slideText === 'Логин') {
      this.router.navigate(['/login']);
    }
    this.currentIndex$.subscribe((index: number) => {
      if (index === 2) {
        this.slideText = 'Логин';
      }
    });
  }

}
