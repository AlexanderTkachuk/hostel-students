import { Injectable } from '@angular/core';
import { UserObject } from '../interfaces/user.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  token: BehaviorSubject<string | boolean> = new BehaviorSubject(false);
  userobject: BehaviorSubject<UserObject | null> = new BehaviorSubject(null);
  constructor() { }
}


