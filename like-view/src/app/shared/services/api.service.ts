import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(url: string, params?: any): Observable<any> {
    return this.http.get(url, {params: params || {}})
    .pipe(
      catchError((error: HttpErrorResponse) => {
        console.log(error);
        return of(error);
      })
    );
  }
  post(url: string, params?: any): Observable<any> {
    return this.http.post(url, params)
    .pipe(
      catchError((error: HttpErrorResponse) => {
        console.log(error);
        return throwError(error);
      })
    );
  }
}
