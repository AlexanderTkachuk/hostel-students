export interface UserObject {
    name: string;
    secondName: string;
    password: string;
    email: string;
    id: string;
    created: string;
    token: string;
  }